package com.insurance.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.insurance.domain.Insurancedata;

public class InsuranceRowMapper implements RowMapper<Insurancedata>{

	@Override
	public Insurancedata mapRow(ResultSet resultset, int arg1) throws SQLException {
		// TODO Auto-generated method stub
		
		InsuranceExtractor insuranceextractor=new InsuranceExtractor();
		return insuranceextractor.extractData(resultset);
	}

}
