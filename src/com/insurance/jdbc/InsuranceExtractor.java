package com.insurance.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.insurance.domain.Insurancedata;

public class InsuranceExtractor implements ResultSetExtractor<Insurancedata>{

	@Override
	public Insurancedata extractData(ResultSet resultset) throws SQLException,
			DataAccessException {
		// TODO Auto-generated method stub
		Insurancedata insurancedata=new Insurancedata();
		insurancedata.setUsername(resultset.getString("username"));
		insurancedata.setPassword(resultset.getString("password"));
		insurancedata.setEmail(resultset.getString("email"));
		return insurancedata;
	}

	

}
