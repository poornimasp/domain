package com.insurance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.insurance.domain.Insurancedata;
import com.insurance.service.Insuranceservice;
import com.insurance.service.InsuranceserviceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
public class Insurance {
	
	
	@Autowired
	Insuranceservice insuranceservice;
	
	@RequestMapping("/adminfunc")
	public ModelAndView adminfunc(){
		
		
		
		return new ModelAndView("/adminlogin");
	}

	
	@RequestMapping("adminregister")
	public ModelAndView adminregister(@RequestParam("username") String username,@RequestParam("email") String email,@RequestParam("password") String password,@RequestParam("confirm-password") String conpassword){
		
		
		
		List<Insurancedata> register=insuranceservice.registeruser(username,email,password,conpassword);
		List<Insurancedata> loginuser=insuranceservice.loginuser();
	/*if(loginuser.get(0).getPassword().equals("conpassword")){
		
	}*/
		
		return new ModelAndView("/adminlogin");
	}


	
}
