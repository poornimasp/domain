package com.insurance.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.insurance.dao.Insurancedao;
import com.insurance.domain.Insurancedata;

import java.util.List;


public class InsuranceserviceImpl implements Insuranceservice{

	@Autowired
	Insurancedao insurancedao;
	
	@Override
	public List<Insurancedata> registeruser(String username, String email,
			String password, String conpassword) {
		// TODO Auto-generated method stub
		return insurancedao.registeruser(username,email,password,conpassword);
	}

	@Override
	public List<Insurancedata> loginuser() {
		// TODO Auto-generated method stub
		return insurancedao.loginuser();
	}

}
