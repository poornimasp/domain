package com.insurance.dao;

import com.insurance.domain.Insurancedata;
import java.util.List;


public interface Insurancedao {

	List<Insurancedata> registeruser(String username, String email,
			String password, String conpassword);

	List<Insurancedata> loginuser();

}
