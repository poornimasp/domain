package com.insurance.dao;

import java.util.ArrayList;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.insurance.domain.Insurancedata;
import com.insurance.jdbc.InsuranceRowMapper;

import java.util.List;


public class InsurancedaoImpl implements Insurancedao{
	
	@Autowired
	DataSource dataSource;
	
	public void setDataSource(DataSource ds) {
        dataSource = ds;
    }
	
	
	
	@Override
	public List<Insurancedata> registeruser(String username, String email,
			String password, String conpassword) {
		// TODO Auto-generated method stub
		
	List<Insurancedata> userregister=new ArrayList<Insurancedata>();
	String sql="insert into insurancetable (username,email,password) values (?,?,?)";
		//List<Guidelines> guidelinesList = new ArrayList<Guidelines>();
		  JdbcTemplate jdbctemplate = new JdbcTemplate(dataSource);
		  jdbctemplate.update(sql, new Object[]{username,email,password});
		return userregister;
		
	}



	@Override
	public List<Insurancedata> loginuser() {
		// TODO Auto-generated method stub
		List<Insurancedata> userlogin=new ArrayList<Insurancedata>();
		String sql="select * from insurancetable ";
		JdbcTemplate jdbctemplate=new JdbcTemplate();
		userlogin=jdbctemplate.query(sql, new InsuranceRowMapper());
		return userlogin;
	}

}
